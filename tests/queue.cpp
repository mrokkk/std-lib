#include <gtest/gtest.h>
#include <queue.h>

using namespace yacppl;

TEST(QueueTests, canCreateEmptyQueue) {
    queue<int> queue;
    auto size = queue.size();
    EXPECT_TRUE(size == 0);
}

TEST(QueueTests, canPushTempElement) {
    queue<int> queue;
    queue.push(439);
    EXPECT_TRUE(queue.size() == 1);
    EXPECT_EQ(queue.front(), 439);
}

TEST(QueueTests, canPushElement) {
    queue<int> queue;
    auto element = 5423;
    queue.push(element);
    EXPECT_TRUE(queue.size() == 1);
    EXPECT_EQ(queue.front(), 5423);
}

TEST(QueueTests, canPushMoreElements) {
    queue<int> queue;
    queue.push(33);
    EXPECT_TRUE(queue.size() == 1);
    EXPECT_EQ(queue.front(), 33);
    queue.push(21);
    EXPECT_TRUE(queue.size() == 2);
    EXPECT_EQ(queue.front(), 33);
    queue.push(594);
    EXPECT_TRUE(queue.size() == 3);
    EXPECT_EQ(queue.front(), 33);
    queue.push(37);
    EXPECT_TRUE(queue.size() == 4);
    EXPECT_EQ(queue.front(), 33);
}

TEST(QueueTests, canPushAndPopElement) {
    queue<int> queue;
    queue.push(439);
    queue.pop();
    EXPECT_TRUE(queue.size() == 0);
}

TEST(QueueTests, canPushAndPopMoreElements) {
    queue<int> queue;
    queue.push(439);
    queue.push(599);
    queue.push(238);
    queue.push(70);
    queue.push(43);
    EXPECT_TRUE(queue.size() == 5);
    EXPECT_EQ(queue.front(), 439);
    queue.pop();
    EXPECT_TRUE(queue.size() == 4);
    EXPECT_EQ(queue.front(), 599);
    queue.pop();
    EXPECT_TRUE(queue.size() == 3);
    EXPECT_EQ(queue.front(), 238);
    queue.pop();
    EXPECT_TRUE(queue.size() == 2);
    EXPECT_EQ(queue.front(), 70);
    queue.pop();
    EXPECT_TRUE(queue.size() == 1);
    EXPECT_EQ(queue.front(), 43);
    queue.pop();
    EXPECT_TRUE(queue.size() == 0);
}

TEST(QueueTests, canPushAndPopByShiftOperators) {
    queue<int> queue;
    int temp = -4;
    queue << temp;
    EXPECT_TRUE(queue.size() == 1);
    EXPECT_EQ(queue.front(), -4);
    queue << 93 << 24;
    EXPECT_TRUE(queue.size() == 3);
    EXPECT_EQ(queue.front(), -4);
    queue << 325 << 0 << 3267;
    EXPECT_TRUE(queue.size() == 6);
    EXPECT_EQ(queue.front(), -4);
    int result[6];
    queue >> result[0];
    EXPECT_TRUE(queue.size() == 5);
    EXPECT_EQ(queue.front(), 93);
    queue >> result[1] >> result[2] >> result[3] >> result[4] >> result[5];
    EXPECT_TRUE(queue.size() == 0);
    EXPECT_EQ(result[0], -4);
    EXPECT_EQ(result[1], 93);
    EXPECT_EQ(result[2], 24);
    EXPECT_EQ(result[3], 325);
    EXPECT_EQ(result[4], 0);
    EXPECT_EQ(result[5], 3267);
}
